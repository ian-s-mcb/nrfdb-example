exports.handler = async function (event, context) {
  const {username} = JSON.parse(event.body)
  return {
    statusCode: 200,
    body: JSON.stringify({ message: `Hello, ${username}` })
  }
}
