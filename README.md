# netlify-functions-hello-world

A tiny React app which greets the user after they pick a name. The
greeting is prepared in a Netlify Function.

### [Demo][demo]

[demo]: https://admiring-mcclintock-f4bbbd.netlify.app/
